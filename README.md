To install this service, follow the steps below:

- Step 1 : Save the file to any folder within your computer
- Step 2 : Open the file, look for LearnSV.exe under LearnSV\bin\Debug file.
- Step 3 : Open Command Prompt by running it as Administrator.
- Step 4 : In the command prompt, type the following command >>

cd c:\Windows\Microsoft.NET\Framework\v4.0.30319

*Note this will require Visual Studio application.
- Step 5 : Follow by the following command >>

installutil [THE LearnSV.exe FILE LOCATION]\LearnSV.exe

Eg: installutil C:\LearnSV\LearnSV\bin\Debug\LearnSV.exe

Then you are set to use the service.

To check whether it is really installed:

- Step 1 : Open "Run" and enter "services.msc" and run it.
- Step 2 : Look for BService

If it is not found... take good care.


To uninstall the service, repeat the step 1 to 4 on install, on step 5 enter the command as below :

installutil -u [THE LearnSV.exe FILE LOCATION]\LearnSV.exe

Eg: installutil -u C:\LearnSV\LearnSV\bin\Debug\LearnSV.exe