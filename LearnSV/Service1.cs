﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace LearnSV
{
    enum Action
    {
        Create,
        Change,
        Rename,
        Delete,
        Error
    }
    public partial class Service1 : ServiceBase
    {
        private static string source = "TestEventLog";
        private static string log = "TestLog";
        EventLog sysEventLog = new EventLog(log);

        private string folder = @"C:\Users\User\Desktop\PDFtest";
        string loc = @"\log";
        string file = @"logService.txt";
        public Service1()
        {
            InitializeComponent();
            CreateFile();
            try
            {
                FSW_pdfTest.Path = folder;
                FSW_pdfTest.IncludeSubdirectories = true;
                FSW_pdfTest.Created += Fsw_Created;
                FSW_pdfTest.Deleted += Fsw_Deleted;
                FSW_pdfTest.Changed += Fsw_Changed;
                FSW_pdfTest.Renamed += Fsw_Renamed;
                FSW_pdfTest.Error += Fsw_Error;
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("OnCreate : " + ex.Message, EventLogEntryType.Error, 100);
            }
        }

        private void Fsw_Error(object sender, ErrorEventArgs e)
        {
            WriteLog(string.Format("Error -  \"{0}\"", e.GetException().Message));
            WriteDB(Action.Error, e.GetException().Message);
        }

        private void Fsw_Renamed(object sender, RenamedEventArgs e)
        {
            FileInfo fi = new FileInfo(e.FullPath);
            if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
            {
                WriteLog(string.Format("Renamed \"{0}\" to \"{1}\"", e.OldFullPath, e.FullPath));
                WriteDB(Action.Rename, e.OldFullPath + " to " + e.FullPath);
            }
        }

        private void Fsw_Changed(object sender, FileSystemEventArgs e)
        {
            try
            {
                FileInfo fi = new FileInfo(e.FullPath);
                if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
                {
                    //prevent changed event called twice
                    FSW_pdfTest.EnableRaisingEvents = false;

                    WriteLog(string.Format("Changed \"{0}\"", e.FullPath));
                    WriteDB(Action.Change, e.FullPath);
                }
            }
            finally
            {
                FSW_pdfTest.EnableRaisingEvents = true;
            }
        }

        private void Fsw_Deleted(object sender, FileSystemEventArgs e)
        {
            WriteLog(string.Format("Deleted \"{0}\"", e.FullPath));
            WriteDB(Action.Delete, e.FullPath);
        }

        private void Fsw_Created(object sender, FileSystemEventArgs e)
        {
            FileInfo fi = new FileInfo(e.FullPath);
            if (!((fi.Attributes | FileAttributes.Hidden) == fi.Attributes))
            {
                WriteLog(string.Format("Created \"{0}\"", e.FullPath));
                WriteDB(Action.Create, e.FullPath);
            }
        }

        protected override void OnStart(string[] args)
        {
            if (args.Length == 1)
            {
                folder = args[0];
            }
            CreateFile();
            try
            {
                using (var w = File.AppendText(folder + loc + @"\" + file))
                {
                    w.WriteLine();
                    w.WriteLine(string.Format("------------------------------", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                    w.WriteLine(string.Format("Service started on {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                    w.Flush();
                    w.Dispose();
                    GC.Collect();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                //WriteLog(string.Format("Error - {0}", ex.Message));
                //ExitCode = 1;
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("OnStart : " + ex.Message, EventLogEntryType.Error, 101);
            }
        }

        protected override void OnStop()
        {
            FSW_pdfTest.EnableRaisingEvents = false;
            try
            {
                using (StreamWriter w = File.AppendText(folder + loc + @"\" + file))
                {
                    w.WriteLine(string.Format("Service stop at {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                    w.WriteLine(string.Format("------------------------", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                    w.WriteLine();
                }
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("OnStop : Fail to write log. " + ex.Message, EventLogEntryType.Error, 105);
            }
        }

        private void WriteLog(string msg)
        {
            try
            {
                using (StreamWriter w = File.AppendText(folder + loc + @"\" + file))
                {
                    w.WriteLine(string.Format("{0} {1} : {2}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), msg));

                    w.Flush();
                    w.Dispose();
                    GC.Collect();
                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                //WriteLog(string.Format("Error - {0}", ex.Message));
                //ExitCode = 1;
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("Write Log : " + ex.Message, EventLogEntryType.Error, 101);
            }
        }

        private void CreateFile()
        {
            try
            {
                if (!Directory.Exists(folder + loc))
                {
                    var di = Directory.CreateDirectory(folder + loc);
                    di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;

                }
                //check file exist
                if (!File.Exists(folder + loc + @"\" + file))
                {
                    using (var strm = File.Create(folder + loc + @"\" + file))
                    {
                        File.SetAttributes(folder + loc + @"\" + file, FileAttributes.Hidden);
                        Byte[] info = new UTF8Encoding(true).GetBytes(string.Format("File created {0} {1}", DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString()));
                        // Add some information to the file.
                        strm.Write(info, 0, info.Length);

                        strm.Flush();
                        strm.Close();
                        strm.Dispose();
                        GC.Collect();
                        GC.Collect();
                    }
                }
            }
            catch (Exception ex)
            {
                //WriteLog(string.Format("Error - {0}", ex.Message));
                //ExitCode = 1;
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("Create File : " + ex.Message, EventLogEntryType.Error, 101);
            }
        }

        private void WriteDB(Action act, string msg)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                using (SqlConnection sqlcon = new SqlConnection(conn))
                {
                    sqlcon.Open();
                    using (SqlCommand cmd = new SqlCommand("insert into logging values(@time,@act,@msg,@mac,@ins)", sqlcon))
                    {
                        cmd.Parameters.AddWithValue("time", DateTime.Now);
                        cmd.Parameters.AddWithValue("act", act.ToString());
                        cmd.Parameters.AddWithValue("msg", msg);
                        cmd.Parameters.AddWithValue("mac", GetMACAddress());
                        cmd.Parameters.AddWithValue("ins", "ServClass");
                        cmd.ExecuteNonQuery();
                    }
                    sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(source))
                    EventLog.CreateEventSource(source, log);
                sysEventLog.Source = source;
                sysEventLog.WriteEntry("WriteDB : " + ex.Message, EventLogEntryType.Error, 101);
            }
        }

        public string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card  
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            }
            return sMacAddress;
        }
    }
}
